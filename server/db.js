const mysql = require('mysql2')

const Pool = mysql.createPool({
  connectionLimit: 20,
  user: 'root',
  password: 'root',
  database: 'cardb',
  port: 3306,
  host: 'cardb',
  waitForConnections: true,
  queueLimit: 0,
})

module.exports = {
  Pool,
}