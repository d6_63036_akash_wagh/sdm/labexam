const express=require('express');
const utils=require('../utils');
const db=require('../db');
const router=express.Router();

router.post('/addcar',(req,res)=>{ 
    const{car_id,car_name, company_name, car_prize}=req.body;
    console.log(car_id);
    const statement=`insert into Car(car_id,car_name, company_name, car_prize) values(?,?,?,?);`;
    db.Pool.query(statement,[car_id,car_name, company_name, car_prize],(err,data)=>{
        res.send(utils.createResult(err,data));
    })
})

router.get('/getallcar', (req,res)=>{//to get all cars on home screen
    const statement=`select * from Car`;
    db.Pool.query(statement,(err,data)=>{
        res.send(utils.createResult(err,data));
    })
})
router.put('/updatecar/:car_id',(req,res)=>{ 
    const {car_id}=req.params;
    const{company_name, car_prize}=req.body;
    const statement=`update Car set company_name=?, car_prize=? where car_id=?;`;
    db.Pool.query(statement,[company_name, car_prize,car_id],(err,data)=>{
        res.send(utils.createResult(err,data));
    })
})
module.exports=router;

router.delete('/deletecar/:car_id',(req,res)=>{ 
    const {car_id}=req.params;
    const statement=`delete from Car where car_id=?;`;
    db.Pool.query(statement,[car_id],(err,data)=>{
        res.send(utils.createResult(err,data));
    })
})