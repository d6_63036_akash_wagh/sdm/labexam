const express =require('express');
const cors =require('cors')
const app=express();
app.use(cors());
app.use(express.json());

const carRouter = require('./routes/car')

app.use('/car', carRouter)
app.listen(5000,'0.0.0.0',()=>{
    console.log('server is started on port 5000');
})